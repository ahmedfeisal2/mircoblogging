<?php

namespace App\Http\Requests;


class RegisterRequest extends RequestBaseAPI
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username'=>'required|regex:/^\S*$/',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|string|confirmed|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/',
            'image'=>'required|image|mimes:jpeg,png|max:1024',

        ];
    }
}
