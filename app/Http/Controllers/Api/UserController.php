<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MakeTweetRequest;
use App\Http\Resources\TweetResource;
use App\Models\Tweet;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function makeTweet(MakeTweetRequest $request){
        $user = auth('api')->user();
        $tweet = $user->myTweets()->create($request->validated());
        return (new TweetResource($tweet))->additional(['status' => 'success','message'=>'']);
    }

    public function follow($id){
        if (!$follower = User::find($id) || $id == auth('api')->id()) {
            return response()->json(['status' => 'fail', 'data' => null, 'message' => 'Follower Not Found'],401);
        }
        $user = auth('api')->user();
        if ($user->following()->find($id)){
            $user->following()->detach($follower);
            return response()->json(['status' => 'success', 'data' => ['is_follow'=>0], 'message' => 'UnFollow Done Successfully'],401);

        }else{
            $user->following()->attach($follower);
            return response()->json(['status' => 'success', 'data' => ['is_follow'=>1], 'message' => 'Follow Done Successfully'],401);

        }
    }

    public function timeLine(){
        $user = auth('api')->user();
        $tweets = Tweet::whereHas('user',function ($q) use ($user){
            return $q->whereIn('users.id',$user->following()->pluck('users.id')->toArray());
        })->paginate(10);
        return (TweetResource::collection($tweets))->additional(['status' => 'success','message'=> '']);
    }
}
