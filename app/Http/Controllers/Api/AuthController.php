<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register(RegisterRequest $request): Response{
        DB::beginTransaction();
        try{
            $user = User::create($request->validated());
            DB::commit();
            $token = \JWTAuth::fromUser($user);
            data_set($user, 'token', $token);
            return response()->json(['status' => 'success','data'=> new UserResource($user) ,'message'=>"تم التسجيل بنجاح ",], 200);

        } catch(\Exception $e){
            DB::rollback();
            \Log::info($e->getMessage());
            return response()->json(['status' => 'fail','data'=> null ,'message'=> "لم يتم التسجيل حاول مرة أخرى"] ,422);
        }

    }
    /**
     * login
     */
    public function login(LoginRequest $request){
        if (!$token = auth('api')->attempt($request->validated())) {
            return response()->json(['status' => 'fail', 'data' => null, 'message' => trans('api.auth.failed')],401);
        }

        $user = auth('api')->user();
        data_set($user,'token' , $token);
        return (new UserResource($user))->additional(['status' => 'success','message'=>'']);
    }




}
