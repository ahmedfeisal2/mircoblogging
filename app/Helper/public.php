<?php

function saveFile($file, $folder)
{
    $fileName =(time()* rand(1, 99)) . '.' . $file->getClientOriginalExtension();

    $dest = storage_path('app/public/'.$folder);
    $file->move($dest, $fileName);

    return  $fileName;
}
